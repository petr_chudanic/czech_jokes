const functions = require('firebase-functions');
const {Storage} = require('@google-cloud/storage');
const { SimpleResponse, dialogflow } = require("actions-on-google");

// Your Google Cloud Platform project ID
const projectId = 'czechjokes-5bfa6';

// Creates a client
const storage = new Storage({
  projectId: projectId,
});
// Instantiate the Dialogflow client.
const app = dialogflow({debug: true});

function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min) ) + min;
}

function sanitizeUrl(url) {
  return url.replace(new RegExp("&", 'g'), "&amp;");
}

app.intent('Czech joke intent', (conv) => {
  console.log(conv.parameters);
  const category = conv.parameters.category;
  const jokeType = conv.parameters.jokeType;

  if (!category && !jokeType) {
    conv.ask("Do you want to tell a joke from some category or random one?");
    return null;
  }

  const bucket = storage.bucket("gs://czechjokes-5bfa6.appspot.com");

  if (jokeType === "category" || category) {
    if (!category) {
      return bucket.getFiles().then((results) => {
        const files = results[0];

        const folders = files.filter(file => {return !file.name.endsWith('.mp3')});
        const categories = folders.reduce((accumulator, currentValue) => {return accumulator + ", " + currentValue.name}, "");
        conv.ask("Select joke category. Available categories are " + categories);

        return null;
      }).catch((e) => {
        console.log("ERROR", e);
      });
    } else {
      return bucket.getFiles().then((results) => {
        const files = results[0];
        const jokes = files.filter(file => {return file.name.endsWith('.mp3') && file.name.startsWith(category)});

        return jokes[getRndInteger(0, jokes.length)];
      }).then((joke) => {
        return joke.getSignedUrl({
          action: 'read',
          expires: '1-1-2020'
        });
      }).then((url) => {
        console.log("URL ", url[0]);
        conv.close(`<speak>Telling joke <audio src='${sanitizeUrl(url[0])}' />Joke</speak>`);
        return null;
      }).catch((e) => {
        console.log("ERROR", e);
      });
    }
  }

  return bucket.getFiles().then((results) => {
    const files = results[0];
    const jokes = files.filter(file => {return file.name.endsWith('.mp3')});

    return jokes[getRndInteger(0, jokes.length)];
  }).then((joke) => {
    return joke.getSignedUrl({
      action: 'read',
      expires: '1-1-2020'
    });
  }).then((url) => {
    console.log("URL ", url[0]);
    conv.close(`<speak>Telling joke <audio src='${sanitizeUrl(url[0])}' />Joke</speak>`);
    return null;
  }).catch((e) => {
    console.log("ERROR", e);
  });

});

// Set the DialogflowApp object to handle the HTTPS POST request.
exports.dialogflowFirebaseFulfillment = functions.https.onRequest(app);
